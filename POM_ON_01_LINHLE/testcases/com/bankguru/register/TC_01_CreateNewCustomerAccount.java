package com.bankguru.register;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import pages.HomePage;
import pages.LoginPage;
import pages.RegisterPage;

public class Register_01_CreateNewAccount extends AbstractTest {
	WebDriver driver;
	private String loginPageURL, userID, password, email;
	
	private LoginPage loginPage;
	private RegisterPage registerPage;
	private HomePage homePage;
	
	@Parameters({"browser", "url"})
	@BeforeClass
	public void beforeClass(String browser, String url)  {
		driver = openBrowser(browser, url);	
		email = "linhle" + randomNumber() + "@gmail.com";
		loginPage = new LoginPage(driver);
		registerPage = new RegisterPage(driver);
		
	}
	
	@Test
	public void Register_01_CreateNewAccountSuccess() {
		loginPageURL = loginPage.getLoginPageURL();
		registerPage = loginPage.clickHereLink();
		registerPage.inputEmail(email);
		registerPage.clickSubmitBtn();
		userID = registerPage.getUserID();
		password = registerPage.getPassword();
	}
	
	@Test
	public void Register_02_LoginSuccessWithAboveInformation() {
		loginPage = registerPage.openLoginPage(loginPageURL);
		loginPage.inputUserID(userID);
		loginPage.inputPassword(password);
		homePage = new HomePage(driver);
		homePage = loginPage.clickLoginBtn();
		String messageLoginSuccess = homePage.getMessageLoginSuccess();
		Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank", messageLoginSuccess.trim());
	}

	@AfterClass
	public void afterClass() {
		closeBrowser();
	}


}
