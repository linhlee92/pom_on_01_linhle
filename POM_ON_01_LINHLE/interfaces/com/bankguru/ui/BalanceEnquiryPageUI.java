package com.bankguru.ui;

public class BalanceEnquiryPageUI {
	public static final String BALANCE = "30000";
	
	public static final String ACCOUNT_TXT_LOC = "//input[@name='accountno']";
	public static final String SUBMIT_BTN_LOC = "//input[@name='AccSubmit']";
	
	public static final String BALANCE_LOC = "//td[contains(text(),'Balance')]/following-sibling::td";
}
