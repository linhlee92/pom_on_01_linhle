package com.bankguru.ui;

public class LoginPageUI {
	
	public static final String HERE_LINK = "//a[contains(.,'here')]";
	public static final String USERID_TXT = "//input[@name='uid']";
	public static final String PASSWORD_TXT = "//input[@name='password']";
	public static final String LOGIN_BTN = "//input[@name='btnLogin']";
}
