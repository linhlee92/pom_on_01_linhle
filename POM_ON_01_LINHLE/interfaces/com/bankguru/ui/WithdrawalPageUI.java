package com.bankguru.ui;

public class WithdrawalPageUI {
	
	// test data
	public static final String AMOUNT = "15000";
	public static final String DESCRIPTION = "Withdraw";
	public static final String MESS_TRANSFER = "Transaction details of Withdrawal for Account ";
	public static final String CURRENT_AMOUNT = "40000";
	
	// location 
	public static final String ACCOUNTNO_LOC = "//input[@name='accountno']";
	public static final String AMOUNT_LOC = "//input[@name='ammount']";
	public static final String DESCRIPTION_LOC = "//input[@name='desc']";
	public static final String SUBMIT_LOC = "//input[@name='AccSubmit']";
	public static final String MESS_TRANSFER_LOC = "//p[contains(text(),'Transaction details of Withdrawal for Account')]";
	public static final String CURRENT_AMOUNT_LOC = "//td[contains(text(),'Current Balance')]/following-sibling::td";
	
}
