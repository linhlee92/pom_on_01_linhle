package com.bankguru.ui;

public class NewCustomerPageUI {
	
	// test data
	public static final String CUSTOMER_NAME = "AUTOMATION TESTING";
	public static final String DOB = "01/01/1989";
	public static final String ADDRESS = "PO Box 911 8331 Duis Avenue";
	public static final String CITY = "Tampa";
	public static final String STATE = "FL";
	public static final String PIN = "466250";
	public static final String MOBILE = "4555442476";
	public static final String EMAIL = "";
	public static final String PASSWORD = "automation";
	
	// location of element
	public static final String CUSTOMERNAME_TXT = "//input[@name='name']";
	public static final String GENDER_RAD = "//td[contains(text(),'Gender')]/following-sibling::td";
	public static final String GENDER_MALE_RAD = "//input[@value='m']";
	public static final String GENDER_FEMALE_RAD = "//input[@value='f']";
	public static final String DOB_TXT = "//*[@id='dob']";
	public static final String ADDRESS_TXT = "//td[contains(text(),'Address')]/following-sibling::td/textarea";
	public static final String CITY_TXT = "//input[@name='city']";
	public static final String STATE_TXT = "//input[@name='state']";
	public static final String PIN_TXT = "//input[@name='pinno']";
	public static final String MOBILENUMBER_TXT = "//input[@name='telephoneno']";
	public static final String EMAIL_TXT = "//input[@name='emailid']";
	public static final String PASSWORD_TXT = "//input[@name='password']";
	public static final String SUBMIT_BTN = "//input[@name='sub']";
	
	public static final String MESS_CREAT_SUCCESS = "Customer Registered Successfully!!!";
	public static final String MESS_CREAT_SUCCESS_LOC = "//p[contains(text(),'Customer Registered Successfully!!!')]";
	
	public static final String CustomerID_LOC = "//td[contains(text(),'Customer ID')]/following-sibling::td";
}
