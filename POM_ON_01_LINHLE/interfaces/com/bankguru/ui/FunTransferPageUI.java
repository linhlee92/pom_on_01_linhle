package com.bankguru.ui;

public class FunTransferPageUI {
	
	// test data
	public static final String OTHER_ACCOUNT_ID = "36730";
	public static final String AMOUNT = "10000";
	public static final String DESCRIPTION = "Transfer";
	public static final String CURRENT_AMOUNT = "10000";
	
	// location 
	public static final String PAYERSACCOUNT_LOC = "//input[@name='payersaccount']";
	public static final String PAYEESACCOUNT_LOC = "//input[@name='payeeaccount']";
	public static final String AMOUNT_LOC = "//input[@name='ammount']";
	public static final String DESCRIPTION_LOC = "//input[@name='desc']";
	public static final String SUBMIT_LOC = "//input[@name='AccSubmit']";
	public static final String FROM_ACCOUNT_LOC = "//td[contains(text(),'From Account Number')]/following-sibling::td";
	public static final String TO_ACCOUNT_LOC = "//td[contains(text(),'To Account Number')]/following-sibling::td";
	public static final String CURRENT_AMOUNT_LOC = "//td[contains(text(),'Amount')]/following-sibling::td";
}
