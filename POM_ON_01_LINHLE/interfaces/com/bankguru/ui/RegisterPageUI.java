package com.bankguru.ui;

public class RegisterPageUI {
	public static final String EMAIL_TXT = "//input[@name='emailid']";
	public static final String SUBMIT_BTN = "//input[@name='btnLogin']";
	public static final String USERID_TEXT = "//td[contains(text(),'User ID :')]/following-sibling::td";
	public static final String PASSWORD_TEXT = "//td[contains(text(),'Password :')]/following-sibling::td";
}
