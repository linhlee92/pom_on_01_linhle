package com.bankguru.ui;

public class DeleteAccountPageUI {
	
	public static final String MESS_DELETE_ACCOUNT_SUCCESS = "Account Deleted Sucessfully";
	
	public static final String ACCOUNT_TXT_LOC = "//input[@name='accountno']";
	public static final String SUBMIT_BTN_LOC = "//input[@name='AccSubmit']";
}
