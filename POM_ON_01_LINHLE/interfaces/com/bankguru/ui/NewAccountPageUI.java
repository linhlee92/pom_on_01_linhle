package com.bankguru.ui;

public class NewAccountPageUI {
	//test data
	public static final String ACCOUNTTYPE = "Current";
	public static final String INITIAL_DEPOSIT = "50000";
	public static final String CURRENTAMOUNT = "50000";
	public static final String MESS_ADDACCOUNT_SUCCESS = "Account Generated Successfully!!!";
	//location
	public static final String CUSTOMERID_TXT_LOC = "//input[@name='cusid']";
	public static final String ACCOUNTTYPE_LOC = "//select";
	public static final String INITIAL_DEPOSIT_LOC = "//input[@name='inideposit']";
	public static final String SUBMIT_BTN_LOC = "//input[@name='button2']";
	public static final String MESS_ADDACCOUNT_SUCCESS_LOC = "//p[text()='Account Generated Successfully!!!']";
	public static final String CURRENTAMOUNT_LOC = "//td[contains(text(),'Current Amount')]/following-sibling::td";
	public static final String ACCOUNT_ID_LOC = "//td[contains(text(),'Account ID')]/following-sibling::td";
}
