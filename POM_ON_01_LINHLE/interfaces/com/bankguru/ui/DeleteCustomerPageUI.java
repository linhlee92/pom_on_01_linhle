package com.bankguru.ui;

public class DeleteCustomerPageUI {

	public static final String MESS_DELETE_CUSTOMER_SUCCESS = "Customer deleted Successfully";

	public static final String CUSTOMER_TXT_LOC = "//input[@name='cusid']";
	public static final String SUBMIT_BTN_LOC = "//input[@name='AccSubmit']";
}
