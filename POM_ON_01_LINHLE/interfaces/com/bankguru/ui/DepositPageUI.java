package com.bankguru.ui;

public class DepositPageUI {
	
	// test data
	public static final String ACCOUNTNO = "5993";
	public static final String AMOUNT = "5000";
	public static final String DESCRIPTION = "Deposit";
	public static final String MESS_TRANSFER = "Transaction details of Deposit for Account ";
	public static final String CURRENT_AMOUNT = "55000";
	
	// location 
	public static final String ACCOUNTNO_LOC = "//input[@name='accountno']";
	public static final String AMOUNT_LOC = "//input[@name='ammount']";
	public static final String DESCRIPTION_LOC = "//input[@name='desc']";
	public static final String SUBMIT_LOC = "//input[@name='AccSubmit']";
	public static final String MESS_TRANSFER_LOC = "//p[contains(text(),'Transaction details of Deposit for Account')]";
	public static final String CURRENT_AMOUNT_LOC = "//td[contains(text(),'Current Balance')]/following-sibling::td";
	
}
