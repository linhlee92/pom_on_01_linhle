package com.bankguru.ui;

import java.util.Random;

public class EditCustomerPageUI {
	
	// test data
	public static final String ADDRESS = "1883 Cursus Avenue";
	public static final String CITY = "Houston";
	public static final String STATE = "Texas";
	public static final String PIN = "166455";
	public static final String MOBILE = "4779728081";
	public static final String EMAIL = "testing" + randomNumber() + "@gmail.com";
	
	// location 
	public static final String CUSTOMERID_TXT = "//input[@name='cusid']";
	public static final String EDIT_SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String ADDRESS_TXT = "//td[contains(text(),'Address')]/following-sibling::td/textarea";
	public static final String CITY_TXT = "//input[@name='city']";
	public static final String STATE_TXT = "//input[@name='state']";
	public static final String PIN_TXT = "//input[@name='pinno']";
	public static final String MOBILENUMBER_TXT = "//input[@name='telephoneno']";
	public static final String EMAIL_TXT = "//input[@name='emailid']";
	public static final String SUBMIT_BTN = "//input[@name='sub']";
	
	public static final String MESS_EDIT_SUCCESS = "Customer details updated Successfully!!!";
	public static final String MESS_EDIT_SUCCESS_LOC = "//p[contains(text(),'Customer details updated Successfully!!!')]";
	
	public static int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(10000);
		return number;
	}
}
