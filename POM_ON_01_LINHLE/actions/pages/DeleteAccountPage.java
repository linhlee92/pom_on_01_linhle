package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DeleteAccountPageUI;

public class DeleteAccountPage extends AbstractPage {
	
	WebDriver driver;
	
	public DeleteAccountPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void inputAccountID(String accountID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, DeleteAccountPageUI.ACCOUNT_TXT_LOC);
		sendkeyToElement(driver, DeleteAccountPageUI.ACCOUNT_TXT_LOC, accountID);
	}

	public void clickSubmitBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, DeleteAccountPageUI.SUBMIT_BTN_LOC);
		clickToElement(driver, DeleteAccountPageUI.SUBMIT_BTN_LOC);
	}
	
	public void acceptDeleteAccount() throws Exception {
		acceptAlert(driver);
	}

	public String getMessDeleteSuccess() throws Exception {
		return getTextAlert(driver);
	}

}
