package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.RegisterPageUI;

public class RegisterPage extends AbstractPage {
	
	WebDriver driver;
	
	public RegisterPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputEmail(String email) {
		waitForControlVisible(driver, RegisterPageUI.EMAIL_TXT);
		sendkeyToElement(driver, RegisterPageUI.EMAIL_TXT, email);
	}

	public void clickSubmitBtn() {
		waitForControlVisible(driver, RegisterPageUI.SUBMIT_BTN);
		clickToElement(driver, RegisterPageUI.SUBMIT_BTN);
	}

	public String getUserID() {
		waitForControlVisible(driver, RegisterPageUI.USERID_TEXT);
		return getTextElement(driver, RegisterPageUI.USERID_TEXT);
	}

	public String getPassword() {
		waitForControlVisible(driver, RegisterPageUI.PASSWORD_TEXT);
		return getTextElement(driver, RegisterPageUI.PASSWORD_TEXT);
	}

	public LoginPage openLoginPage(String url) {
		openURL(driver, url);
		return PageFactory.openLoginPage(driver);
	}

}
