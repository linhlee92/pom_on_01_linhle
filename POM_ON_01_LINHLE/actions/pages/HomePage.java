package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.HomePageUI;

public class HomePage extends AbstractPage {
	
	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getMessageLoginSuccess() {
		waitForControlVisible(driver, HomePageUI.MESSAGE_TEXT);
		return getTextElement(driver, HomePageUI.MESSAGE_TEXT);
	}
}
