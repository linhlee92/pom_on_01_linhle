package pages;

import org.openqa.selenium.WebDriver;

public class PageFactory {
	
	public static LoginPage openLoginPage(WebDriver driver) {
		return new LoginPage(driver);
	}
	
	public static RegisterPage openLoginRegisterPage(WebDriver driver) {
		return new RegisterPage(driver);
	}
	
	public static HomePage openHomePage(WebDriver driver) {
		return new HomePage(driver);
	}
	// 14 pages
	public static NewCustomerPage openNewCustomerPage(WebDriver driver) {
		return new NewCustomerPage(driver);
	}
	
	public static EditCustomerPage openEditCustomerPage(WebDriver driver) {
		return new EditCustomerPage(driver);
	}
	
	public static DeleteCustomerPage openDeleteCustomerPage(WebDriver driver) {
		return new DeleteCustomerPage(driver);
	}
	
	public static NewAccountPage openNewAccountPage(WebDriver driver) {
		return new NewAccountPage(driver);
	}
	
	public static EditAccountPage openEditAccountPage(WebDriver driver) {
		return new EditAccountPage(driver);
	}
	
	public static DeleteAccountPage openDeleteAccountPage(WebDriver driver) {
		return new DeleteAccountPage(driver);
	}
	
	public static DepositPage openDepositPage(WebDriver driver) {
		return new DepositPage(driver);
	}
	
	public static WithdrawalPage openWithdrawalPage(WebDriver driver) {
		return new WithdrawalPage(driver);
	}
	
	public static FunTransferPage openFunTransferPage(WebDriver driver) {
		return new FunTransferPage(driver);
	}
	
	public static ChangePasswordPage openChangePasswordPage(WebDriver driver) {
		return new ChangePasswordPage(driver);
	}
	
	public static BalanceEnquiryPage openBalanceEnquiryPage(WebDriver driver) {
		return new BalanceEnquiryPage(driver);
	}
	
	public static MiniStatementPage openMiniStatementPage(WebDriver driver) {
		return new MiniStatementPage(driver);
	}
	
	public static CustomisedStatementPage openCustomisedStatementPage(WebDriver driver) {
		return new CustomisedStatementPage(driver);
	}
	
	public static LogoutPage openLogoutPage(WebDriver driver) {
		return new LogoutPage(driver);
	}
}
