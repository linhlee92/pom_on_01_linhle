package pages;

import org.openqa.selenium.WebDriver;
import com.bankguru.ui.LoginPageUI;

public class LoginPage extends AbstractPage {
	
	WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getLoginPageURL() {
		return currentURL(driver);
	}

	public RegisterPage clickHereLink() {
		waitForControlVisible(driver, LoginPageUI.HERE_LINK);
		clickToElement(driver, LoginPageUI.HERE_LINK);
		return PageFactory.openLoginRegisterPage(driver);
	}

	public void inputUserID(String userID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		sendkeyToElement(driver, LoginPageUI.USERID_TXT, userID);
	}

	public void inputPassword(String password) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, LoginPageUI.PASSWORD_TXT);
		sendkeyToElement(driver, LoginPageUI.PASSWORD_TXT, password);
	}
	
	public HomePage clickLoginBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		return PageFactory.openHomePage(driver);
	}
	
}
