package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.EditCustomerPageUI;

public class EditCustomerPage extends AbstractPage {

	WebDriver driver;

	public EditCustomerPage(WebDriver driver)  {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void inputCustomerID(String customerID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMERID_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.CUSTOMERID_TXT, customerID);
	}

	public void clickEditSubmitBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.EDIT_SUBMIT_BTN);
		clickToElement(driver, EditCustomerPageUI.EDIT_SUBMIT_BTN);
	}

	public void editAddress(String address) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.ADDRESS_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.ADDRESS_TXT, address);
	}

	public void editCity(String city) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.CITY_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.CITY_TXT, city);
	}

	public void editState(String state) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.STATE_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.STATE_TXT, state);
	}

	public void editPIN(String pin) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.PIN_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.PIN_TXT, pin);
	}

	public void editMobile(String mobile) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.MOBILENUMBER_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.MOBILENUMBER_TXT, mobile);
	}

	public void editEmail(String email) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.EMAIL_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.EMAIL_TXT, email);
	}
	
	public void clickSubmitBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, EditCustomerPageUI.SUBMIT_BTN);
		clickToElement(driver, EditCustomerPageUI.SUBMIT_BTN);
	}

	public String getMessCreatSuccess() {
		waitForControlVisible(driver, EditCustomerPageUI.MESS_EDIT_SUCCESS_LOC);
		return getTextElement(driver, EditCustomerPageUI.MESS_EDIT_SUCCESS_LOC);
	}
}
