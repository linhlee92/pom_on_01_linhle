package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DeleteCustomerPageUI;

public class DeleteCustomerPage extends AbstractPage {

	WebDriver driver;

	public DeleteCustomerPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void inputCustomerID(String customerID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, DeleteCustomerPageUI.CUSTOMER_TXT_LOC);
		sendkeyToElement(driver, DeleteCustomerPageUI.CUSTOMER_TXT_LOC, customerID);
	}

	public void clickSubmitBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, DeleteCustomerPageUI.SUBMIT_BTN_LOC);
		clickToElement(driver, DeleteCustomerPageUI.SUBMIT_BTN_LOC);
	}

	public void acceptDeleteCustomer() throws Exception {
		acceptAlert(driver);
	}

	public String getMessDeleteSuccess() throws Exception {
		// TODO Auto-generated method stub
		return getTextAlert(driver);
	}
	

}
