package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.FunTransferPageUI;

public class FunTransferPage extends AbstractPage {
	
	WebDriver driver;

	public FunTransferPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void inputPayersAccountNo(String accountID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.PAYERSACCOUNT_LOC);
		sendkeyToElement(driver, FunTransferPageUI.PAYERSACCOUNT_LOC, accountID);
	}

	public void inputPayeesAccountNo(String otherAccountID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.PAYEESACCOUNT_LOC);
		sendkeyToElement(driver, FunTransferPageUI.PAYEESACCOUNT_LOC, otherAccountID);
	}

	public void inputAmount(String amount) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.AMOUNT_LOC);
		sendkeyToElement(driver, FunTransferPageUI.AMOUNT_LOC, amount);
	}
	
	public void inputDescription(String description) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.DESCRIPTION_LOC);
		sendkeyToElement(driver, FunTransferPageUI.DESCRIPTION_LOC, description);
	}

	public void clickSubmitBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.SUBMIT_LOC);
		clickToElement(driver, FunTransferPageUI.SUBMIT_LOC);
	}

	public String getFromAccount() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.FROM_ACCOUNT_LOC);
		return getTextElement(driver, FunTransferPageUI.FROM_ACCOUNT_LOC);
	}

	public String getToAccount() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.TO_ACCOUNT_LOC);
		return getTextElement(driver, FunTransferPageUI.TO_ACCOUNT_LOC);
	}

	public String getAmount() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, FunTransferPageUI.CURRENT_AMOUNT_LOC);
		return getTextElement(driver, FunTransferPageUI.CURRENT_AMOUNT_LOC);
	}
	
	
}
