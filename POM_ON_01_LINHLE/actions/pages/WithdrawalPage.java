package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.WithdrawalPageUI;

public class WithdrawalPage extends AbstractPage {
	
	WebDriver driver;
	
	public WithdrawalPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	public void inputAccountNo(String accountID) {
		waitForControlVisible(driver, WithdrawalPageUI.ACCOUNTNO_LOC);
		sendkeyToElement(driver, WithdrawalPageUI.ACCOUNTNO_LOC, accountID);
	}

	public void inputAmount(String amount) {
		waitForControlVisible(driver, WithdrawalPageUI.AMOUNT_LOC);
		sendkeyToElement(driver, WithdrawalPageUI.AMOUNT_LOC, amount);
	}

	public void inputDescription(String description) {
		waitForControlVisible(driver, WithdrawalPageUI.DESCRIPTION_LOC);
		sendkeyToElement(driver, WithdrawalPageUI.DESCRIPTION_LOC, description);
	}

	public void clickSubmitBtn() {
		waitForControlVisible(driver, WithdrawalPageUI.SUBMIT_LOC);
		clickToElement(driver, WithdrawalPageUI.SUBMIT_LOC);
	}

	public String getMessTransfer() {
		waitForControlVisible(driver, WithdrawalPageUI.MESS_TRANSFER_LOC);
		return getTextElement(driver, WithdrawalPageUI.MESS_TRANSFER_LOC);
	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, WithdrawalPageUI.CURRENT_AMOUNT_LOC);
		return getTextElement(driver, WithdrawalPageUI.CURRENT_AMOUNT_LOC);
	}
}
