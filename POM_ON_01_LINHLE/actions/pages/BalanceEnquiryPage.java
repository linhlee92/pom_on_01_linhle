package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.BalanceEnquiryPageUI;

public class BalanceEnquiryPage extends AbstractPage {
	
	WebDriver driver;
	
	public BalanceEnquiryPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public String getBalanceAccount() {
		waitForControlVisible(driver, BalanceEnquiryPageUI.BALANCE_LOC);
		return getTextElement(driver, BalanceEnquiryPageUI.BALANCE_LOC);
	}

	public void inputAccountID(String accountID) {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, BalanceEnquiryPageUI.ACCOUNT_TXT_LOC);
		sendkeyToElement(driver, BalanceEnquiryPageUI.ACCOUNT_TXT_LOC, accountID);
	}

	public void clickSubmitBtn() {
		// TODO Auto-generated method stub
		waitForControlVisible(driver, BalanceEnquiryPageUI.SUBMIT_BTN_LOC);
		clickToElement(driver, BalanceEnquiryPageUI.SUBMIT_BTN_LOC);
	}

}
