package pages;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bankguru.ui.AbstractPageUI;

public class AbstractPage {

	WebDriver driver;
	private int time_outs = 30;

	// WEB BROWSER
	public void openURL(WebDriver driver, String url) {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	public String getTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String currentURL(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public void backToPage(WebDriver driver) {
		driver.navigate().back();
	}

	public void forwardToPage(WebDriver driver) {
		driver.navigate().forward();
	}

	public void refreshAnyPage(WebDriver driver) {
		driver.navigate().refresh();
	}

	// WEB ELEMENT
	public void clickToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clickToElement(WebDriver driver, String locator, String value) {
		String dynamicLocator = String.format(locator, value);
		WebElement element = driver.findElement(By.xpath(dynamicLocator));
		element.click();
	}

	public void sendkeyToElement(WebDriver driver, String locator, String text) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.clear();
		element.sendKeys(text);
	}
	
	public void sendkeyToElementNoClear(WebDriver driver, String locator, String text) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(text);
	}

	public void selectItemInDropdown(WebDriver driver, String locator, String text) {
		Select select = new Select(driver.findElement(By.xpath(locator)));
		select.selectByVisibleText(text);
	}

	public void getFirstItemSelected(WebDriver driver, String locator) {
		Select select = new Select(driver.findElement(By.xpath(locator)));
		select.getFirstSelectedOption();
	}

	public String getAttributeElement(WebDriver driver, String locator, String attribute) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attribute);
	}

	public String getTextElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getText();
	}

	public int getSizeElement(WebDriver driver, String locator) {
		List<WebElement> elements = driver.findElements(By.xpath(locator));
		return elements.size();
	}

	public void uncheckTheCheckbox(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (element.isSelected()) {
			element.click();
		}
	}

	public boolean isControlDisplayed(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isDisplayed();
	}

	public boolean isControlDisplayed(WebDriver driver, String locator, String value) {
		String dynamicLocator = String.format(locator, value);
		WebElement element = driver.findElement(By.xpath(dynamicLocator));
		return element.isDisplayed();
	}

	public boolean isControlSelected(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isSelected();
	}

	public boolean isControlEnabled(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isEnabled();
	}

	public void acceptAlert(WebDriver driver) throws Exception {
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void cancelAlert(WebDriver driver) throws Exception {
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public String getTextAlert(WebDriver driver) throws Exception {
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		return alert.getText();
	}

	public void sendkeyToAlert(WebDriver driver, String text) throws Exception {
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(text);
	}

	public void switchToWindowByID(WebDriver driver, String parentID) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			if (!childWindows.equals(parentID)) {
				driver.switchTo().window(childWindows);
				break;
			}
		}
	}

	public void swithToWindowByTitle(WebDriver driver, String childrenTitle) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindow : allWindows) {
			driver.switchTo().window(childWindow);
			String childTitle = driver.getTitle();
			if (childTitle.equals(childrenTitle)) {
				break;
			}
		}
	}

	public void switchToIframe(WebDriver driver, String locator) {
		WebElement iframe = driver.findElement(By.xpath(locator));
		driver.switchTo().frame(iframe);
	}

	public void backToDefaultContent(WebDriver driver) {
		driver.switchTo().defaultContent();
	}

	public void doubleClick(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.doubleClick(element);
	}

	public void hoverMouse(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.clickAndHold(element);
	}

	public void rightClick(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.contextClick(element);
	}

	public void dragAndDrop(WebDriver driver, String loc_DragElement, String loc_DropElement) {
		WebElement dragElement = driver.findElement(By.xpath(loc_DragElement));
		WebElement dropElement = driver.findElement(By.xpath(loc_DropElement));
		Actions action = new Actions(driver);
		action.dragAndDrop(dragElement, dropElement).build().perform();
	}

	public void keyDownElement(WebDriver driver, String locator, Keys modifierKey) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.keyDown(element, modifierKey);
	}

	public void keyUpElement(WebDriver driver, String locator, Keys modifierKey) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.keyUp(element, modifierKey);
	}

	public void sendKeyElement(WebDriver driver, String locator, String keysToSend) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.sendKeys(element, keysToSend);
	}

	public void uploadFile(WebDriver driver, String locator, String filePath) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(filePath);
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForWebElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	
	public static void highlightElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
	}

	public void waitForControlPresence(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, time_outs);
		By by = By.xpath(locator);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public void waitForControlVisible(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, time_outs);
		By by = By.xpath(locator);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void waitForControlVisible(WebDriver driver, String locator, String value) {
		String dynamicLocator = String.format(locator, value);
		WebDriverWait wait = new WebDriverWait(driver, time_outs);
		By by = By.xpath(dynamicLocator);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void waitForControlClickable(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, time_outs);
		By by = By.xpath(locator);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public void waitForControlNotVisible(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, time_outs);
		By by = By.xpath(locator);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	public void waitForAlertPresence(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, time_outs);
		wait.until(ExpectedConditions.alertIsPresent());
	}
	
	// 14 pages in Home Page
	public NewCustomerPage openNewCustomerPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "New Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "New Customer");
		return PageFactory.openNewCustomerPage(driver);
	}
	
	public EditCustomerPage openEditCustomerPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Edit Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Edit Customer");
		return PageFactory.openEditCustomerPage(driver);
	}
	
	public DeleteCustomerPage openDeleteCustomerPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Delete Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Delete Customer");
		return PageFactory.openDeleteCustomerPage(driver);
	}
	
	public NewAccountPage openNewAccountPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "New Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "New Account");
		return PageFactory.openNewAccountPage(driver);
	}
	
	public EditAccountPage openEditAccountPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Edit Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Edit Account");
		return PageFactory.openEditAccountPage(driver);
	}
	
	public DeleteAccountPage openDeleteAccountPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Delete Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Delete Account");
		return PageFactory.openDeleteAccountPage(driver);
	}
	
	public DepositPage openDepositPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Deposit");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Deposit");
		return PageFactory.openDepositPage(driver);
	}
	
	public WithdrawalPage openWithdrawalPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Withdrawal");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Withdrawal");
		return PageFactory.openWithdrawalPage(driver);
	}
	
	public FunTransferPage openFunTransferPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Fund Transfer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Fund Transfer");
		return PageFactory.openFunTransferPage(driver);
	}
	
	public ChangePasswordPage openChangePasswordPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Change Password");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Change Password");
		return PageFactory.openChangePasswordPage(driver);
	}
	
	public BalanceEnquiryPage openBalanceEnquiryPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Balance Enquiry");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Balance Enquiry");
		return PageFactory.openBalanceEnquiryPage(driver);
	}
	
	public MiniStatementPage openMiniStatementPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Mini Statement");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Mini Statement");
		return PageFactory.openMiniStatementPage(driver);
	}
	
	public CustomisedStatementPage openCustomisedStatementPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Customised Statement");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Customised Statement");
		return PageFactory.openCustomisedStatementPage(driver);
	}
	
	public LogoutPage openLogoutPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LOCATION, "Log out");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LOCATION, "Log out");
		return PageFactory.openLogoutPage(driver);
	}
}
