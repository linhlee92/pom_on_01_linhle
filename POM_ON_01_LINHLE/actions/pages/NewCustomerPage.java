package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.NewCustomerPageUI;

public class NewCustomerPage extends AbstractPage {
	
	WebDriver driver;
	
	public NewCustomerPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void inputCustomerName(String customerName) {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMERNAME_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.CUSTOMERNAME_TXT, customerName);
	}
	
	public void chooseMaleGender() {
		waitForControlVisible(driver, NewCustomerPageUI.GENDER_RAD);
		if(!isControlSelected(driver, NewCustomerPageUI.GENDER_MALE_RAD)) {
			clickToElement(driver, NewCustomerPageUI.GENDER_MALE_RAD);
		}
	}
	
	public void chooseFemaleGender() {
		waitForControlVisible(driver, NewCustomerPageUI.GENDER_RAD);
		if(!isControlSelected(driver, NewCustomerPageUI.GENDER_FEMALE_RAD)) {
			clickToElement(driver, NewCustomerPageUI.GENDER_FEMALE_RAD);
		}
	}
	
	public void inputDateOfBirth(String DOB) {
		waitForControlVisible(driver, NewCustomerPageUI.DOB_TXT);
		sendkeyToElementNoClear(driver, NewCustomerPageUI.DOB_TXT, NewCustomerPageUI.DOB);
	}
	
	public void inputAddress(String address) {
		waitForControlVisible(driver, NewCustomerPageUI.ADDRESS_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.ADDRESS_TXT, address);
	}
	
	public void inputCity(String city) {
		waitForControlVisible(driver, NewCustomerPageUI.CITY_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.CITY_TXT, city);
	}
	
	public void inputState(String state) {
		waitForControlVisible(driver, NewCustomerPageUI.STATE_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.STATE_TXT, state);
	}
	
	public void inputPIN(String pin) {
		waitForControlVisible(driver, NewCustomerPageUI.PIN_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.PIN_TXT, pin);
	}
	
	public void inputMobileNumber(String mobilenumber) {
		waitForControlVisible(driver, NewCustomerPageUI.MOBILENUMBER_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.MOBILENUMBER_TXT, mobilenumber);
	}
	
	public void inputEmail(String emailCustomer) {
		waitForControlVisible(driver, NewCustomerPageUI.EMAIL_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.EMAIL_TXT, emailCustomer);
	}
	
	public void inputPassword(String passwordCustomer) {
		waitForControlVisible(driver, NewCustomerPageUI.PASSWORD_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.PASSWORD_TXT, passwordCustomer);
	}
	
	public void clickSubmitBtn() {
		waitForControlVisible(driver, NewCustomerPageUI.SUBMIT_BTN);
		clickToElement(driver, NewCustomerPageUI.SUBMIT_BTN);
	}
	
	public String getMessCreatSuccess() {
		waitForControlVisible(driver, NewCustomerPageUI.MESS_CREAT_SUCCESS_LOC);
		return getTextElement(driver, NewCustomerPageUI.MESS_CREAT_SUCCESS_LOC);
	}
	
	public String getCustomerID() {
		waitForControlVisible(driver, NewCustomerPageUI.CustomerID_LOC);
		return getTextElement(driver, NewCustomerPageUI.CustomerID_LOC);
	}
}
