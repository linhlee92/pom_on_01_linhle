package pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DepositPageUI;

public class DepositPage extends AbstractPage {
	
	WebDriver driver;

	public DepositPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void inputAccountNo(String accountID) {
		waitForControlVisible(driver, DepositPageUI.ACCOUNTNO_LOC);
		sendkeyToElement(driver, DepositPageUI.ACCOUNTNO_LOC, accountID);
	}

	public void inputAmount(String amount) {
		waitForControlVisible(driver, DepositPageUI.AMOUNT_LOC);
		sendkeyToElement(driver, DepositPageUI.AMOUNT_LOC, amount);
	}

	public void inputDescription(String description) {
		waitForControlVisible(driver, DepositPageUI.DESCRIPTION_LOC);
		sendkeyToElement(driver, DepositPageUI.DESCRIPTION_LOC, description);
	}

	public void clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.SUBMIT_LOC);
		clickToElement(driver, DepositPageUI.SUBMIT_LOC);
	}

	public String getMessTransfer() {
		waitForControlVisible(driver, DepositPageUI.MESS_TRANSFER_LOC);
		return getTextElement(driver, DepositPageUI.MESS_TRANSFER_LOC);
	}
	
	public String getCurrentAmount() {
		waitForControlVisible(driver, DepositPageUI.CURRENT_AMOUNT_LOC);
		return getTextElement(driver, DepositPageUI.CURRENT_AMOUNT_LOC);
	}
}
