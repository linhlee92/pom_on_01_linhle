package SeleniumTestNG;

import org.testng.annotations.Test;

public class TestNG_02_Priority {
	
	@Test(groups = "linhlee", priority = 3 , enabled = false)
	public void TC01() {
		System.out.println("Test Case 01 _ LINHLEE");
	}

	@Test(groups = "bimbim" , priority = 1 , enabled = true)
	public void TC02() {
		System.out.println("Test Case 02 _ BIMBIM");
	}
	
	@Test(groups = "bonbon" , priority = 2 , enabled = true)
	public void TC03() {
		System.out.println("Test Case 03 _ BONBON");
	}
	
}
