package SeleniumTestNG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNG_03_Parameter {

	WebDriver driver;

	@Parameters({"browser"})
	@BeforeClass
	public void initBrowser(String browser) {
		if (browser.equals("ff")) {
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equals("ie")){
			System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
	}
	
	@Parameters({"url"})
	@Test
	public void Test(String url) {
		driver.get(url);
	}
	
	@AfterClass
	public void closeBrowser() {
		driver.quit();
	}
	
}
