package SeleniumTestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNG_05_MultiBrowsers {
	
	WebDriver driver;
	
	@Parameters({"browser"})
	@BeforeClass
	public void initBrowser(String browser) {
		if (browser.equals("ff")) {
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equals("ie")){
			System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
	}
	
	@DataProvider(name = "User/Pass")
	public static Object[][] userAndPassword() {
		return new Object[][] {{"mngr111644", "enEdagy"},{"mngr111645", "YsEnEva"}};
	}
	
	@Test(dataProvider="User/Pass")
	public void Test(String username, String password) throws Exception {
		driver.get("http://demo.guru99.com/v4/");

		WebElement inputUsername = driver.findElement(By.xpath("//input[@name='uid']"));
		inputUsername.sendKeys(username);

		WebElement inputPassword = driver.findElement(By.xpath("//input[@name='password']"));
		inputPassword.sendKeys(password);

		WebElement btnClick = driver.findElement(By.xpath("//input[@name='btnLogin']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", btnClick);
		Thread.sleep(3000);

		String messSuccess = driver.findElement(By.xpath("//marquee")).getText();
		Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank", messSuccess.trim());
	}
	
	@AfterClass
	public void closeBrowser() {
		driver.quit();
	}
}
