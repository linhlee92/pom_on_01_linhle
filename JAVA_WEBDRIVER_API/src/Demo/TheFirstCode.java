package Demo;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TheFirstCode {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	@Test
	public void TC_02() {
		// Step 01
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Step 02
		driver.get("http://live.guru99.com/index.php/customer/account/login/");

		// Step 03 - Để trống Username/ Password -  Click Login button
		driver.findElement(By.xpath("//*[@id='send2']")).click();

		// Step 05 
		String emailError = driver.findElement(By.xpath("//*[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("This is a required field.", emailError);
		
		String passwordError = driver.findElement(By.xpath("//*[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals("This is a required field.", passwordError);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
