package ThePractice;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_08_upload_files {

	WebDriver driver;
	String path = "E:\\Trainning_Selenium\\JAVA_WEBDRIVER_API\\fileToUpload\\uploadFile.jpg";
	String fileName = "uploadFile.jpg";
	private Scanner input_browser;

	@BeforeClass
	public void beforeClass() {
		input_browser = new Scanner(System.in);
		System.out.println("Choose browser you want to test : ");
		String browser = input_browser.nextLine();
		if (browser.equals("ff")) {
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equals("ie")) {
			System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
	}
	
	@Test
	public void TC01_SendKeys_API() throws Exception {
		// step 01 : go to website
		driver.get("http://www.helloselenium.com/2015/03/how-to-upload-file-using-sendkeys.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 02 : sendkey to upload file
		WebElement uploadFiles = driver.findElement(By.xpath("//input[@name='uploadFileInput']"));
		uploadFiles.sendKeys(path);
		Thread.sleep(3000);

		// step 03 : click button upload
		WebElement uploadBtn = driver.findElement(By.xpath("//input[@name='uploadFileButton']"));
		uploadBtn.click();
	}
	
	@Test
	public void TC02_AutoIT() throws Exception {
		// step 01 : go to website
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 02 : upload file
		// ff
		WebElement uploadFilesFF = driver.findElement(By.xpath("//input[@type='file']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", uploadFilesFF);	
			
		// ie
//		WebElement uploadFilesIE = driver.findElement(By.xpath("//span[contains(text(),'Add files...')]"));
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("arguments[0].click();", uploadFilesIE);	
		
		// chrome
//		WebElement uploadFilesChrome = driver.findElement(By.cssSelector(".fileinput-button"));
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("arguments[0].click();", uploadFilesChrome);	

		Runtime.getRuntime().exec(new String[] { ".\\fileToUpload\\firefox.exe", path });
		Thread.sleep(3000);

		// step 03 : check file
		boolean result = driver
				.findElement(By.xpath(
						"//table[@role='presentation']//" + "p[@class='name' and contains(text(),'" + fileName + "')]"))
				.isDisplayed();
		Assert.assertTrue(result);
	}

	@Test
	public void TC03_RobotClass() throws Exception {
		// step 01 : go to website
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 02 : Use robot class for ie/chrome/ff
		// Specify the file location with extension
		StringSelection select = new StringSelection(path);

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Click
		driver.findElement(By.className("fileinput-button")).click();

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
