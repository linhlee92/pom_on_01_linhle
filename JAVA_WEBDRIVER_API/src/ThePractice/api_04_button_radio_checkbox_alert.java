package ThePractice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_04_button_radio_checkbox_alert {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	
	public void TC01_Button() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		WebElement button = driver.findElement(By.xpath("//*[@id='button-enabled']"));
		button.click();
		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());

		driver.navigate().back();

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", button);
		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());
	}

	
	public void TC02_Checkbox() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		WebElement checkbox = driver.findElement(
				By.xpath("//ul[@class='fieldlist']/li/label[text()='Dual-zone air conditioning']/../input"));

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", checkbox);
		Assert.assertTrue(checkbox.isSelected());

		if (checkbox.isSelected()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", checkbox);
			Assert.assertFalse(checkbox.isSelected());
		}
	}

	@Test
	public void TC03_Radiobutton() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		WebElement radiobtn = driver
				.findElement(By.xpath("//ul[@class='fieldlist']/li/label[text()='2.0 Petrol, 147kW']/../input"));

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radiobtn);
		Assert.assertTrue(radiobtn.isSelected());

		if (radiobtn.isSelected()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", radiobtn);
			Assert.assertFalse(radiobtn.isSelected());
		}
	}

	@Test
	public void TC04_JsAlert() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[text()='Click for JS Alert']")).click();

		Alert alert = driver.switchTo().alert();
		String alertMsg = alert.getText();
		Assert.assertEquals("I am a JS Alert", alertMsg);
		alert.accept();
		Assert.assertEquals("You clicked an alert successfully",
				driver.findElement(By.xpath("//*[@id='result']")).getText());
	}
	
	@Test
	public void TC05_JsConfirm() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[text()='Click for JS Confirm']")).click();

		Alert alert = driver.switchTo().alert();
		String alertMsg = alert.getText();
		Assert.assertEquals("I am a JS Confirm", alertMsg);
		alert.dismiss();
		Assert.assertEquals("You clicked: Cancel",
				driver.findElement(By.xpath("//*[@id='result']")).getText());
	}
	
	@Test
	public void TC06_JsPrompt() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();

		Alert alert = driver.switchTo().alert();
		String alertMsg = alert.getText();
		Assert.assertEquals("I am a JS prompt", alertMsg);
		String text = "linh lee";
		alert.sendKeys(text);
		alert.accept();
		Assert.assertEquals("You entered: " + text,
				driver.findElement(By.xpath("//*[@id='result']")).getText());
	}
	
	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
