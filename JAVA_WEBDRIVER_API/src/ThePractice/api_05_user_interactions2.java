package ThePractice;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_05_user_interactions2 {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	@Test
	public void TC01_hover_case01() {
		// step 1 : go to website
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// step 2 : hover to "Hover over me"
		Actions action = new Actions(driver);
		WebElement hover = driver.findElement(By.xpath("//a[contains(text(),'Hover over me')]"));
		action.moveToElement(hover).perform();
		// step 3 : Verify tooltip
		String text = driver.findElement(By.xpath("//div[@class='tooltip-inner']")).getText();
		Assert.assertEquals("Hooray!", text);
	}

	@Test
	public void TC01_hover_case02() {
		// step 1 : go to website
		driver.get("http://www.myntra.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// Step 2 : hover to Menu
		WebElement account = driver.findElement(By.xpath("//div[@class='desktop-userIconsContainer']"));
		Actions action = new Actions(driver);
		action.moveToElement(account).perform();
		// step 3 :click login
		WebElement login = driver.findElement(By.xpath("//a[contains(text(),'login')]"));
		login.click();
		// Step 4 : verify form login is displayed
		WebElement formLogin = driver.findElement(By.xpath("//div[@class='login-box']"));
		if (formLogin.isDisplayed()) {
			System.out.println("Login Form is displayed");
		} else {
			System.out.println("Login Form is not displayed");
		}
	}

	@Test  // ERROR TC02_clickAndHold !!!
	public void TC02_clickAndHold() throws Exception {
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// step 2 : click and hold from 1 to 4
		List<WebElement> lists = driver.findElements(By.xpath("//*[@id='selectable']/li"));
		Actions action = new Actions(driver);
		action.clickAndHold(lists.get(0)).clickAndHold(lists.get(3)).click().perform();
		action.release();
		// step 3 : verify
		List<WebElement> numberSelected = driver
				.findElements(By.xpath("//li[@class='ui-state-default ui-selectee ui-selected']"));
		int number = numberSelected.size();
		Assert.assertEquals(4, number);
	}

	@Test
	public void TC03_doubleclick() {
		// step 1 : go to webstie
		driver.get("http://www.seleniumlearn.com/double-click");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// step 2 : double click
		Actions action = new Actions(driver);
		WebElement doubleClick = driver.findElement(By.xpath("//button[contains(text(),'Click Me!')]"));
		action.doubleClick(doubleClick).perform();
		action.release();
		// Step 3 : verify text at the alert
		Alert alert = driver.switchTo().alert();
		String text = alert.getText();
		Assert.assertEquals("The Button was double-clicked.", text);
		alert.accept();
	}

	@Test
	public void TC04_rightClick() {
		// step 1 : go to website
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// step 2 : right click on "right click me"
		Actions action = new Actions(driver);
		WebElement rightClick = driver.findElement(By.xpath("//span[contains(text(),'right click me')]"));
		action.contextClick(rightClick).perform();
		// step 3 : hover to Quit
		WebElement quit = driver.findElement(By.xpath("//li/span[contains(text(),'Quit')]"));
		action.moveToElement(quit).perform();
		// step 4 : verify quit is visible
		WebElement quitIsVisible = driver
				.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(.,'Quit')]"));
		if(quitIsVisible.isDisplayed()) {
			System.out.println("Quit is visible");
		} else {
			System.out.println("Quit is not visible");
		}
		// step 5 : click quit
		quit.click();
		// step 6 : accept Js alert
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	@Test
	public void TC05_drapAndDrop_case01() {
		// step 1 : go to website
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// step 2 : drag small circle on big circle
		Actions action = new Actions(driver);
		WebElement smallCircle = driver.findElement(By.xpath("//*[@id='draggable']"));
		WebElement bigCircle = driver.findElement(By.xpath("//*[@id='droptarget']"));
		action.dragAndDrop(smallCircle, bigCircle).perform();
		action.release();
		// step 3 : verify message
		String mess = driver.findElement(By.xpath("//*[@id='droptarget']")).getText();
		Assert.assertEquals("You did great!", mess);
	}
	
	@Test
	public void TC05_drapAndDrop_case02() {
		// step 1 : go to website
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// step 2 : drag small circle on big circle
		Actions action = new Actions(driver);
		WebElement drag = driver.findElement(By.xpath("//*[@id='draggable']"));
		WebElement target = driver.findElement(By.xpath("//*[@id='droppable']"));
		action.dragAndDrop(drag, target).perform();
		action.release();
		// step 3 : verify message
		String mess = driver.findElement(By.xpath("//*[@id='droppable']/p")).getText();
		Assert.assertEquals("Dropped!", mess);
	}
	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
