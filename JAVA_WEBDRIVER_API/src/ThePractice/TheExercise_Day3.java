package ThePractice;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TheExercise_Day3 {
	WebDriver driver;
	String HomePage = "http://live.guru99.com/";
	String AccountPage = "http://live.guru99.com/index.php/customer/account/";
	String RegisterPage = "http://live.guru99.com/index.php/customer/account/create/";
	String loc_txtEmail = "//*[@id='email']";
	String loc_txtPassword = "//*[@id='pass']";
	String loc_btnLogin = "//*[@id='send2']";
	String loc_linkMyAccount = "//div[@class='footer']//a[contains(text(),'My Account')]";
	String loc_btnCreateAccount = "//a[@title='Create an Account']";

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@Test
	public void TC01_VerifyURLandTitle() throws Exception {
		driver.get(HomePage);
		// check title
		String titlePage = driver.getTitle();
		Assert.assertEquals("Home page", titlePage);
		// go to log in page
		driver.get(AccountPage);
		// go to register account
		driver.get(RegisterPage);
		// back to log in page
		driver.navigate().back();
		// check url log in
		String LoginURL = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/", LoginURL);
		// forward to register accout
		driver.navigate().forward();
		String registerURL = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/create/", registerURL);
	}
	@Test
	public void TC02_LoginEmpty() {
		// go to http://live.guru99.com/
		driver.get(HomePage);

		// go to http://live.guru99.com/index.php/customer/account/
		driver.get(AccountPage);

		// empty login and password
		driver.findElement(By.xpath(loc_txtEmail));

		driver.findElement(By.xpath(loc_txtPassword));

		// click button Login
		driver.findElement(By.xpath(loc_btnLogin)).click();

		// verify notification
		String errorEmail = driver.findElement(By.xpath("//*[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("This is a required field.", errorEmail);

		String errorPassword = driver.findElement(By.xpath("//*[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals("This is a required field.", errorPassword);
	}

	@Test
	public void TC03_LoginWithEmailInvalid() {
		// go to http://live.guru99.com/
		driver.get(HomePage);

		// go to http://live.guru99.com/index.php/customer/account/
		driver.get(AccountPage);

		// input invalid email
		driver.findElement(By.xpath(loc_txtEmail)).sendKeys("123434234@12312.123123");

		// click button login
		driver.findElement(By.xpath(loc_btnLogin)).click();

		// verify notification
		String actual_notification = "Please enter a valid email address. For example johndoe@domain.com.";
		String expected_notification = driver.findElement(By.xpath("//div[@id='advice-validate-email-email']"))
				.getText();
		Assert.assertEquals(actual_notification, expected_notification);
	}

	@Test
	public void TC04_LoginWithPasswordIncorrect() throws Exception {
		// go to http://live.guru99.com/
		driver.get(HomePage);

		// go to http://live.guru99.com/index.php/customer/account/
		driver.get(AccountPage);

		// input correct email
		driver.findElement(By.xpath(loc_txtEmail)).sendKeys("automation@gmail.com");

		// input incorrect password
		driver.findElement(By.xpath(loc_txtPassword)).sendKeys("123");

		// click button login
		driver.findElement(By.xpath(loc_btnLogin)).click();

		// verify notification
		String actual_notification = "Please enter 6 or more characters. Leading or trailing spaces will be ignored.";
		String expected_notification = driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']"))
				.getText();
		Assert.assertEquals(actual_notification, expected_notification);
	}

	@Test
	public void TC05_CreateAccount() {
		// go to http://live.guru99.com/
		driver.get(HomePage);

		// go to My Account
		driver.findElement(By.xpath(loc_linkMyAccount)).click();

		// go to Create Account
		driver.findElement(By.xpath(loc_btnCreateAccount)).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Input correct information
		// first name
		driver.findElement(By.xpath("//*[@id='firstname']")).sendKeys("linh");
		// mid name
		driver.findElement(By.xpath("//*[@id='middlename']")).sendKeys("tuan");
		// last name
		driver.findElement(By.xpath("//*[@id='lastname']")).sendKeys("le");
		// email
		driver.findElement(By.xpath("//*[@id='email_address']")).sendKeys("linhle" + random() + "@gmail.com");
		// password
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("01101992");
		// confirm password
		driver.findElement(By.xpath("//*[@id='confirmation']")).sendKeys("01101992");
		// Sign Up for Newsletter
		driver.findElement(By.xpath("//*[@id='is_subscribed']")).click();

		// click button Register
		driver.findElement(By.xpath("//button[@title='Register']")).click();

		// verify message
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String actual = "Thank you for registering with Main Website Store.";
		String expected = driver.findElement(By.xpath("//li[@class='success-msg']//span")).getText();
		Assert.assertEquals(actual, expected);
	}
	
	public int random() {
		// TODO Auto-generated method stub
		Random random = new Random();
		int number = random.nextInt();
		return number;
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
