package ThePractice;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EXERCISE_Day4 {
	
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void TC_01_IsDisplayed() {
		System.out.println("====== Test Case 01 ======");
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// is displayed : email/ Age (Under 18)/ Education
		WebElement email = driver.findElement(By.xpath("//label[contains(text(),'Email:')]"));
		if (email.isDisplayed()) {
			System.out.println("Element is displayed");
			WebElement txt_email = driver.findElement(By.xpath("//*[@id='mail']"));
			txt_email.clear();
			txt_email.sendKeys("linhle" + randomEmail() + "@gmail.com");
		} else {
			System.out.println("Element is not displayed");
		}

		WebElement age = driver.findElement(By.xpath("//label[contains(text(),'Age:')]"));
		if (age.isDisplayed()) {
			System.out.println("Element is displayed");
			WebElement rdo_age_under_18 = driver.findElement(By.xpath("//*[@id='under_18']"));
			if (rdo_age_under_18.isEnabled()) {
				System.out.println("Element is enabled");
			} else {
				rdo_age_under_18.click();
			}
		} else {
			System.out.println("Element is not displayed");
		}

		WebElement education = driver.findElement(By.xpath("//label[contains(text(),'Education')]"));
		if (education.isDisplayed()) {
			System.out.println("PElement is displayed");
			WebElement txt_education = driver.findElement(By.xpath("//*[@id='edu']"));
			txt_education.sendKeys("DHCNHN");
		} else {
			System.out.println("Element is not displayed");
		}
	}
	
	@Test
	public void TC_02_IsEnabled() {
		System.out.println("====== Test Case 02 ======");
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		// is enabled
		WebElement email = driver.findElement(By.xpath("//*[@id='mail']"));
		WebElement age_under_18 = driver.findElement(By.xpath("//*[@id='under_18']"));
		WebElement education = driver.findElement(By.xpath("//*[@id='edu']"));
		WebElement job_role_01 = driver.findElement(By.xpath("//*[@id='job1']"));
		WebElement interests_development = driver.findElement(By.xpath("//*[@id='development']"));
		WebElement slider_01 = driver.findElement(By.xpath("//*[@id='slider-1']"));
		WebElement btn_is_enabled = driver.findElement(By.xpath("//*[@id='button-enabled']"));

		if (email.isEnabled() && age_under_18.isEnabled() && education.isEnabled() && job_role_01.isEnabled()
				&& interests_development.isEnabled() && slider_01.isEnabled() && btn_is_enabled.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		/*
		if (email.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (age_under_18.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (education.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (job_role_01.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (interests_development.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (slider_01.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (btn_is_enabled.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		*/
		
		// is disabled
		WebElement password = driver.findElement(By.xpath("//*[@id='password']"));
		WebElement age_rdo_is_disabled = driver.findElement(By.xpath("//*[@id='radio-disabled']"));
		WebElement biography = driver.findElement(By.xpath("//*[@id='bio']"));
		WebElement job_role_02 = driver.findElement(By.xpath("//*[@id='job2']"));
		WebElement interests_cb_is_disabled = driver.findElement(By.xpath("//*[@id='check-disbaled']"));
		WebElement slider_02 = driver.findElement(By.xpath("//*[@id='slider-2']"));
		WebElement btn_is_disabled = driver.findElement(By.xpath("//*[@id='button-disabled']"));
		
		if (password.isEnabled() && age_rdo_is_disabled.isEnabled() && biography.isEnabled() && job_role_02.isEnabled()
				&& interests_cb_is_disabled.isEnabled() && slider_02.isEnabled() && btn_is_disabled.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		/*
		if (password.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (age_rdo_is_disabled.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (biography.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (job_role_02.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (interests_cb_is_disabled.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (slider_02.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		
		if (btn_is_disabled.isEnabled()) {
			System.out.println("Element is enabled");
		} else {
			System.out.println("Element is disabled");
		}
		*/
	}
	
	@Test
	public void TC03_IsSelected() throws InterruptedException {
		System.out.println("====== Test Case 03 ======");
		driver.get("http://daominhdam.890m.com/");

		// click age under 18 && interests (development)
		WebElement age_under_18 = driver.findElement(By.xpath("//*[@id='under_18']"));
		age_under_18.click();
		WebElement interests_development = driver.findElement(By.xpath("//*[@id='development']"));
		interests_development.click();
		
		// check is selected . if not, click to select
		if(age_under_18.isSelected() && interests_development.isSelected()){
			System.out.println("Element is selected");
		} else {
			System.out.println("Element is not selected");
			age_under_18.click();
			interests_development.click();
		}
	}

	public int randomEmail() {
		// TODO Auto-generated method stub
		Random random = new Random();
		int number = random.nextInt();
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
