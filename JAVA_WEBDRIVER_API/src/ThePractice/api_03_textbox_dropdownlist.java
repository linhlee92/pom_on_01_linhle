package ThePractice;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class api_03_textbox_dropdownlist {

	private WebDriver driver;
	public String user, pass_01, customerName, dateOfBirth, address_01, city, state, pin, userID, mobile, email,
			pass_02;

	public String newAddress, newCity;

	@BeforeTest
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		user = "mngr101234";
		pass_01 = "sYgabad";
		customerName = "Linh Lee";
		dateOfBirth = "01/10/1992";
		address_01 = "4 ngo 52 duong Cau Giay";
		city = "Ha Noi";
		state = "Cau Giay";
		pin = "012345";
		mobile = "01626527672";
		email = "linhle" + random() + "@gmail.com";
		newAddress = "4 ngo 52";
		newCity = "Tokyo";
		pass_02 = "01101992";
	}

	@Test
	public void TC01_DropDownList() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		Select select = new Select(driver.findElement(By.xpath("//*[@id='job1']")));
		// verify dropdownlist doesn't support multi-select
		Assert.assertFalse(select.isMultiple());
		// select "Automation Tester" by selectVisible
		select.selectByVisibleText("Automation Tester");
		WebElement value01 = select.getFirstSelectedOption();
		System.out.println(value01.getText());
		Assert.assertEquals(value01.getText(), "Automation Tester");
		Thread.sleep(3000);
		
		// select "Manual Tester" by selectValue
		select.selectByValue("manual");
		WebElement value02 = select.getFirstSelectedOption();
		System.out.println(value02.getText());
		Assert.assertEquals(value02.getText(), "Manual Tester");
		Thread.sleep(3000);
		
		// select "Mobile Tester" by selectIndex
		select.selectByIndex(3);
		WebElement value03 = select.getFirstSelectedOption();
		System.out.println(value03.getText());
		Assert.assertEquals(value03.getText(), "Mobile Tester");

		// verify value of dropdownlist
		int value_of_dropdownlist = select.getOptions().size();
		Assert.assertEquals(value_of_dropdownlist, 5);
	}

	@Test
	public void TC02_TextBox_TextArea() throws Exception {

		// step 1 : go to "http://demo.guru99.com/v4"
		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// step 2 : log in with user, pass
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(user);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pass_01);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

		// step 3 : click new customer
		driver.findElement(By.xpath("//a[contains(text(),'New Customer')]")).click();

		// step 4 : input data
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys(customerName);
		driver.findElement(By.xpath("//input[@name='rad1']")).click();
		driver.findElement(By.xpath("//*[@id='dob']")).sendKeys(dateOfBirth);
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(address_01);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(city);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(state);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(pin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(mobile);
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pass_02);
		
		// click submit
		Thread.sleep(300);
		driver.findElement(By.xpath("//input[@name='sub']")).click();
		
		// step 5 : get customer ID
		
		String customerID = driver.findElement(By.xpath("//td[text()='Customer ID']/following-sibling::td")).getText();

		// Step 6 : choose Edit Customer -> input customerID -> submit
		driver.findElement(By.xpath("//a[contains(text(),'Edit Customer')]")).click();
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(customerID);
		driver.findElement(By.xpath("//input[@name='AccSubmit']")).click();

		// Step 7 : verify 2 value customer name and address
		String cusName = driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value");
		String address_02 = driver.findElement(By.xpath("//textarea[@name='addr']")).getText();
		Assert.assertEquals(cusName, customerName);
		Assert.assertEquals(address_02, address_01);

		// input new data
	
		WebElement changeAddress = driver.findElement(By.xpath("//textarea[@name='addr']"));
		changeAddress.clear();
		changeAddress.sendKeys(newAddress);

		WebElement changeCity = driver.findElement(By.xpath("//input[@name='city']"));
		changeCity.clear();
		changeCity.sendKeys(newCity);
		
		// click submit to edit
		driver.findElement(By.xpath("//input[@name='sub']")).click();

		// verify value after change
		Thread.sleep(500);
		String addressAfterEdit = driver.findElement(By.xpath("//td[text()='Address']/following-sibling::td")).getText();
		Assert.assertEquals(addressAfterEdit, newAddress);
		String cityAfterEdit = driver.findElement(By.xpath("//td[text()='City']/following-sibling::td")).getText();
		Assert.assertEquals(cityAfterEdit, newCity);
	}
	
	public int random() {
		Random random = new Random();
		int number = random.nextInt(1000);
		return number;
	}
	
	@AfterTest
	public void afterClass() {
		driver.quit();
	}

}
