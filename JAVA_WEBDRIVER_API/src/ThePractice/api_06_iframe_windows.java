package ThePractice;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_06_iframe_windows {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
//		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
		driver = new FirefoxDriver();
	}

	@Test
	public void TC01_iframe() {
		// step 01 : go to website : "https://www.hdfcbank.com/"
		driver.get("https://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		// step 02 : verify text "What are you looking for?"
		WebElement iframe01 = driver.
				findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(iframe01);
		WebElement messageText = driver.findElement(By.xpath("//*[@id='messageText']"));
		String messExpected = messageText.getText();
		Assert.assertEquals("What are you looking for?", messExpected);
		
		// back to default content
		driver.switchTo().defaultContent();
		
		// step 03 : verify banner image ( 6 images )
		WebElement iframe02 = driver.
				findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(iframe02);
		List <WebElement> bannerImage = driver.findElements(By.xpath("//div[@class='bannerimage-container']"));
		int number = bannerImage.size();
		Assert.assertEquals(6, number);
		
		// back to default content
		driver.switchTo().defaultContent();
		
		// step 04 : verify flipper banner 
		WebElement flipBanner = driver.findElement(By.xpath("//div[@class='flipBanner']"));
		Assert.assertTrue(flipBanner.isDisplayed());
	}
	
	@Test
	public void TC02_windows() {
		// step 01 : go to website : "http://daominhdam.890m.com/"
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		// step 02 : click "Opening a new window: Click Here"
		driver.findElement(By.xpath("//a[contains(text(),'Click Here')]")).click();
		String parentID = driver.getWindowHandle();
		switchToWindowByID(parentID);
		
		// step 03 : verify title of new window
		String titleChildWindow = driver.getTitle();
		Assert.assertEquals("Google", titleChildWindow);
		
		// step 04 : close new window
		closeAllWindowsWithoutParent(parentID);
	}
	
	@Test
	public void TC03_multiwindows() {
		// step 01 : go to website :"http://www.hdfcbank.com/"
		driver.get("http://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		// step 02 : close advertisement ( if has )
		
		// step 03 : click Angri link
		String parentID = driver.getWindowHandle();
		driver.findElement(By.xpath("//div[@class='sectionnav']//a[contains(text(),'Agri')]")).click();
		swithToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra");
		
		// step 04 : click Account Details link
		driver.findElement(By.xpath("//p[contains(text(),'Account Details')]")).click();
		swithToWindowByTitle("Welcome to HDFC Bank NetBanking");
		
		// step 05 : click Privacy Policy link
		WebElement frame = driver.findElement(By.xpath("//frame[@name='footer']"));
		driver.switchTo().frame(frame);
		driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]")).click();
		swithToWindowByTitle(
				"HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");
		
		// step 06 : click CSR link
		driver.findElement(By.xpath("//div[@class='hygeinenav']//a[text()='CSR']")).click();
		
		// step 07 : back to main window
		closeAllWindowsWithoutParent(parentID);
	}
	
	@AfterClass
	public void afterClass() {
		driver.quit();
	}
	
	public void switchToWindowByID(String parentID) {
		Set<String> allWindows = driver.getWindowHandles();   
		for (String childWindows : allWindows)   
		{
			if(!childWindows.equals(parentID))   
			{
				driver.switchTo().window(childWindows);  
				break;
			}
		}
	}
	
	public void swithToWindowByTitle(String childrenTitle) {
		Set<String> allWindows = driver.getWindowHandles();  
		for (String childWindow : allWindows)  
		{
			driver.switchTo().window(childWindow);  
			String childTitle = driver.getTitle();   
			if(childTitle.equals(childrenTitle))     
			{
				break;
			}
		}
	}
	
	public boolean closeAllWindowsWithoutParent(String parentID) {
		Set<String> allWindows = driver.getWindowHandles(); 
		for(String childWindow : allWindows) 
		{
			if(!childWindow.equals(parentID)) 
			{
				driver.switchTo().window(childWindow);
				driver.close(); 
			}
		}
		// switch to parent Window
		driver.switchTo().window(parentID);
		
		if (driver.getWindowHandles().size() == 1) 
		{  
			return true; 
		} else {
			return false;
		}
	}
	
}
