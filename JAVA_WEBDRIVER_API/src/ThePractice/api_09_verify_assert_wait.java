package ThePractice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_09_verify_assert_wait {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
	}

	@Test
	public void TC01_ImplicitWait() throws Exception {
		// Step 01 : go to website
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		// Wait for 2 seconds before throw an exception
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement startButton = driver.findElement(By.xpath("//button[text()='Start']"));
		startButton.click();

		WebElement helloText = driver.findElement(By.xpath("//h4[text()='Hello World!']"));
		String text = helloText.getText();
		Assert.assertEquals("Hello World!", text);
	}

	@Test
	public void TC02_ExplicitWait() {
		// Step 01 : go to website
		driver.get(
				"http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// steop 02
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Panel1']")));
		
		// step 03
		WebElement textBeforeClick = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_ctl00_ContentPlaceholder1_Label1Panel']"));
		Assert.assertEquals("No Selected Dates to display.", textBeforeClick.getText().trim());
		
		// step 04 : 
		WebElement today = driver.findElement(By.xpath("//td[a[text()='12']]"));
		today.click();
		
		// step 05 : 
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='raDiv']")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@class='rcSelected']//a[text()='12']")));
		
		Assert.assertEquals("Tuesday, December 12, 2017", textBeforeClick.getText().trim());
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
