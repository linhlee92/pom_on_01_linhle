package ThePractice;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class api_07_javascript_executor {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		Scanner input_browser = new Scanner(System.in);
		System.out.println("Choose browser you want to test : ");
		String browser = input_browser.nextLine();
		if (browser.equals("ff")) {
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equals("ie")) {
			System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void TC01_JavaScriptExecutor() throws Exception {
		// step 01 : go to website "http://live.guru99.com/"
		driver.get("http://live.guru99.com/");

		// step 02 : get domain page
		String titlePage = (String) executeForBrowserElement(driver, "return document.domain");
		Assert.assertEquals("live.guru99.com", titlePage);

		// step 03 : get url
		String URL = (String) executeForBrowserElement(driver, "return document.URL");
		Assert.assertEquals("http://live.guru99.com/", URL);

		// step 04 : open mobile page
		WebElement mobilePage = driver.findElement(By.xpath("//a[text()='Mobile']"));
		executeForWebElement(driver, mobilePage);
		Thread.sleep(3000);
		
		// step 05 : add sp Ss galaxy vao cart
		WebElement ssGalaxyCartButton = driver.findElement(By.xpath(
				"//h2[a[contains(text(),'Samsung Galaxy')]]/following-sibling::div[@class='actions']//button[contains(.,'Add to Cart')]"));
		highlightElement(driver, ssGalaxyCartButton);
		executeForWebElement(driver, ssGalaxyCartButton);
		Thread.sleep(3000);
		
		// step 06 : verify message
		String innerText = (String) executeForBrowserElement(driver, "return document.documentElement.innerText;");
		Assert.assertTrue(innerText.contains("Samsung Galaxy was added to your shopping cart."));

		// step 07 : Open PRIVACY POLICY page
		WebElement privacyPolicyLink = driver.findElement(By.xpath("//a[text()='Privacy Policy']"));
		executeForWebElement(driver, privacyPolicyLink);
		Thread.sleep(3000);
		String titlePrivacyPolicyPage = (String) executeForBrowserElement(driver, "return document.title");
		Assert.assertEquals("Privacy Policy", titlePrivacyPolicyPage);

		// step 08 : Srcoll to end page
		scrollToBottomPage(driver);

		// step 09 : verify text
		WebElement lastItem = driver.findElement(By.xpath(
				"//th[contains(text(),'WISHLIST_CNT')]/following-sibling::td[contains(.,'The number of items in your Wishlist.')]"));
		Assert.assertTrue(lastItem.isDisplayed());
		Thread.sleep(3000);
		
		// step 10 :
		executeForBrowserElement(driver, "return window.location = 'http://demo.guru99.com/v4/'");
		String domainGuruPage = (String) executeForBrowserElement(driver, "return document.domain");
		Assert.assertEquals("demo.guru99.com", domainGuruPage);
	}
	
	@Test
	public void TC02_removeAttribute() throws Exception {
		// step 01 : go to URL
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
				
		// step 02 : remove attribute disabled of field Last Name
		WebElement iframe = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(iframe);
		WebElement lastName = driver.findElement(By.xpath("//input[@name='lname']"));
		removeAttributeInDOM(driver, lastName, "disabled");
		
		// step 03 : Sendkey to field Last name
		executeForWebElement(driver, lastName);
		lastName.sendKeys("Automation Testing");
		Thread.sleep(3000);
		
		// step 04 : Click Submit button
		WebElement submitBtn = driver.findElement(By.xpath("//input[@value='Submit']"));
		executeForWebElement(driver, submitBtn);
		Thread.sleep(3000);
		
		// Step 05 : verify text contains "Automation Testing"
		String innerText = (String) executeForBrowserElement(driver, "return document.documentElement.innerText;");
		Assert.assertTrue(innerText.contains("fname=&lname=Automation Testing"));
	}
	
	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForWebElement(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebDriver driver, WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public static void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
	}
}
